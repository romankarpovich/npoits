package main

import (
	"encoding/json"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"net/http/httptest"
	"npoIts/app"
	"npoIts/db"
	"os"
	"testing"
)

type testItem struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Metric int    `json:"metric"`
}

func TestMain(m *testing.M) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalln(err)
	}
	app.InitRouter()
	db.InitDb()

	existTable()
	code := m.Run()

	os.Exit(code)
}

func existTable() {
	if err := db.Store.Ping(); err != nil {
		log.Fatal(err)
	}
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func execRequest(request *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	app.Router.ServeHTTP(rr, request)
	return rr
}

func TestGetItems(t *testing.T) {
	req, _ := http.NewRequest("GET", "/items", nil)
	resp := execRequest(req)
	checkResponseCode(t, http.StatusOK, execRequest(req).Code)

	m := make(map[string][]testItem)
	err := json.Unmarshal(resp.Body.Bytes(), &m)
	if err != nil {
		t.Errorf(err.Error())
	}

	items, ok := m["items"]
	if !ok {
		t.Errorf("No items!")
	}

	testItems := make([]testItem, 0)
	testItems = append(testItems, testItem{
		ID: "cfa0dae8-e1f3-4964-a203-607febf0db1a",
		Name: "item11",
		Metric: 532,
	})

	testItems = append(testItems, testItem{
		ID: "232d939f-6911-46e3-a6d7-14795fe522a6",
		Name: "item2",
		Metric: 172,
	})

	testItems = append(testItems, testItem{
		ID: "241a033e-c26b-4b06-acae-3e21fb2dd0be",
		Name: "item6",
		Metric: 91,
	})

	testItems = append(testItems, testItem{
		ID: "79169435-584d-4208-8a9c-5374a8ce95d6",
		Name: "item3",
		Metric: 75,
	})

	testItems = append(testItems, testItem{
		ID: "8acb0aa9-a69c-4c47-b5a6-ab5a04aade33",
		Name: "item21",
		Metric: 52,
	})

	for i, v := range items {
		if v.ID == testItems[i].ID && v.Name == testItems[i].Name && v.Metric == testItems[i].Metric {
			continue
		} else {
			t.Errorf("Inequality")
		}
	}
}