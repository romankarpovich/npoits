# RESTapi

После выполнения первой команды необходимо обновить данные в .env-файле для выбора типа хранилища и соединения с БД
```bash
cp .env.example .env
docker build -t rest-api-app .
docker run -dp 8080:8080 rest-api-app
```