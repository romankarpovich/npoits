package db

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	_ "github.com/lib/pq"
)

var Store *sql.DB

func InitDb() {
	conn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_NAME"),
		)

	s, err := sql.Open("postgres", conn)
	if err != nil {
		log.Fatal(err)
	}

	err = s.Ping()
	if err != nil {
		log.Fatal(err)
	}

	Store = s
}
