package app

import (
	"github.com/julienschmidt/httprouter"
	"net/http"
	"npoIts/models"
)

var Router *httprouter.Router

func InitRouter() {
	Router = new(httprouter.Router)
	Router.GET("/items", index)
}

func index(writer http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	if request.Method != http.MethodGet {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	items := new(models.Items)

	err := items.Get()
	if err != nil {
		Failed(writer, http.StatusNotFound, err)
		return
	}

	Success(writer, http.StatusOK, items)

}