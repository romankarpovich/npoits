package models

import (
	"database/sql"
	"log"
	"npoIts/db"
)

type getter interface {
	Get() error
}

type Items struct {
	Values []*Item `json:"items"`
	getter
}

type Item struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Metric int    `json:"metric"`
}

func (i *Items) Get() error {
	rows, err := db.Store.Query("SELECT id, name, metric FROM item6 ORDER BY metric DESC LIMIT 5;")
	if err != nil {
		return err
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			log.Println(err)
		}
	}(rows)

	for rows.Next() {
		item := new(Item)
		if err := rows.Scan(&item.ID, &item.Name, &item.Metric); err != nil {
			return err
		}
		i.Values = append(i.Values, item)
	}
	return nil
}
