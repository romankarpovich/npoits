FROM golang:latest
WORKDIR /opt/app
COPY . .
RUN go mod tidy && go build -o ./main
CMD ["./main"]
EXPOSE 8080