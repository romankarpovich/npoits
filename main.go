package main

import (
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"npoIts/app"
	"npoIts/db"
)

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalln(err)
	}

	db.InitDb()
	app.InitRouter()
}

func main() {
	log.Println("App starts on port :8080")
	err := http.ListenAndServe(":8080", app.Router)
	if err != nil {
		log.Fatalln(err)
	}
}
